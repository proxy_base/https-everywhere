#!/bin/bash

source_encryptedweb=https://github.com/squarefractal/encryptedweb
source_https_everywhere=https://github.com/EFForg/https-everywhere
PATCH_PB_COM=0001-add-proxy-base.com-rules.patch
PATCH_PB_ORG=0002-add-proxy-base.org-rules.patch
CUSTOM_FOLDER_ENCWEB_PB=encryptedweb-PB
CUSTOM_FOLDER_HTTPS_EVERYWHERE_PB=https-everywhere-PB

rm -rf bin/*

git clone ${source_encryptedweb} ${CUSTOM_FOLDER_ENCWEB_PB}
cd ${CUSTOM_FOLDER_ENCWEB_PB}
git submodule init
git submodule update
patch -p1 < ../${PATCH_PB_COM}
patch -p1 < ../${PATCH_PB_ORG}
./makexpi.sh
cp -R pkg/* ../bin/
cd ..
rm -rf ${CUSTOM_FOLDER_ENCWEB_PB}


git clone ${source_https_everywhere} ${CUSTOM_FOLDER_HTTPS_EVERYWHERE_PB}
cd ${CUSTOM_FOLDER_HTTPS_EVERYWHERE_PB}
git submodule init
git submodule update
patch -p1 < ../${PATCH_PB_COM}
patch -p1 < ../${PATCH_PB_ORG}
./makexpi.sh
./makecrx.sh
cp -R pkg/https-everywhere* ../bin/
cd ..
rm -rf ${CUSTOM_FOLDER_HTTPS_EVERYWHERE_PB}

for fsum in bin/*; do sha512sum ${fsum} > ${fsum}.sha512; done
git add .
git cm "auto rebuild"
git push
